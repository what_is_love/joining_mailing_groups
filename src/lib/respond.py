"""
Module for repond-object for Joiner and Sender.

Vladislav Bolshakov (c). All rights reserved.
"""


# standart library
from typing import Union, Any


class Respond:
    def __init__(
            self, success: bool = True, respond_text: str = None,
            respond_list: list = None,
            respond_exception: Exception = None) -> None:
        """
        Class which helps to hold results of execution of any function.

        ### Args:
            - success (bool, optional): success rate of execution of function.\
                 Defaults to True.
            - respond_text (str, optional): respond text of execution \
                (shows to user). Defaults to None.
            - respond_list (list, optional): some list for responding. \
                elements depends on function. Defaults to None.
            - respond_exception (Exception, optional): if exception appears \
                during execution. Defaults to None.

        ### Methods:
            - add_respond_object() - appends object to response list.
            #### --- Getters ---

            - get_success() - gets the success rate of execution.
            - get_respond_text() - returns the respond text of execution \
                if such exists.
            - get_respond_exception() - returns the respond exception of \
                execution if such exists.
            - get_respond_list() - gets list of returning objects for \
                responding.
            #### --- Setters ---
            - set_success()
            - set_respond_text()
            - set_respond_exception()
        """
        # success rate of execution of function
        self.__success = success
        # respond text of execution (shows to user)
        self.__respond_text = respond_text
        # some list for responding.
        if respond_list is None:
            self.__respond_list = []
        else:
            self.__respond_list = respond_list
        # if exception appears during execution
        self.__respond_exception = respond_exception

    def add_respond_object(self, respond_object: Any):
        """appends object to response list.

        Args:
            respond_object (Any): object to append.
        """
        self.__respond_list.append(respond_object)

    # ------- Getters ------- #
    def get_success(self) -> bool:
        """gets the success rate of execution.

        Returns:
            bool: success or not
        """
        return self.__success

    def get_respond_text(self) -> Union[str, None]:
        """returns the respond text of execution if such exists.

        Returns:
            Union[str, None]: respond text of execution
        """
        return self.__respond_text

    def get_respond_exception(self) -> Union[Exception, None]:
        """returns the respond exception of execution if such exists.

        Returns:
            Union[Exception, None]: respond exception of execution
        """
        return self.__respond_exception

    def get_respond_list(self) -> list:
        """gets list of returning objects for responding.

        Returns:
            list: list of returning objects.
        """
        return self.__respond_list

    # ------- Setters ------- #
    def set_success(self, success: bool):
        self.__success = success

    def set_respond_text(self, respond_text: str):
        self.__respond_text = respond_text

    def set_respond_exception(self, respond_exception: Exception):
        self.__respond_exception = respond_exception
