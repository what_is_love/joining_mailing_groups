"""
Module for Telethon client with additional parameters.

Vladislav Bolshakov (c). All rights reserved.
"""


# stanart library
import traceback
import asyncio

# standart library
from typing import List, Union
from datetime import datetime
from random import uniform

# global imports
from telethon import TelegramClient
from telethon.tl.functions.messages import ImportChatInviteRequest
from telethon.errors.rpcerrorlist import FloodWaitError, PeerFloodError,\
    UserAlreadyParticipantError, InviteHashExpiredError, ChannelsTooMuchError,\
    InviteRequestSentError
from telethon.types import Dialog, Message, InputPeerUser
from telethon.tl.functions.updates import GetStateRequest

# local imports
from .respond import Respond


class NewClient:
    def __init__(
            self, telethon_client: TelegramClient,
            groups_to_join: List[str],
            delay_between_sending: int = 120,
            delay_before_sending: int = 0,
            delay_between_joining: int = 10) -> None:

        # cleint for working with telegram
        self.telethon_client = telethon_client
        # delay (sec) between sending messages to groups
        self.__delay_between_sending = delay_between_sending
        # delay (mins) before starting mailing
        self.__delay_before_sending = delay_before_sending*60
        # time till wait before start sending
        self.__timeout_before_sending = None
        # groups which client should enter
        self.__groups_to_join = groups_to_join
        # is the client have spam block on joining groups
        self.__group_enter_spam: bool = False
        # time till account should wait before joining
        self.__group_enter_timeout: float = None
        # delay between joinning each group
        self.__delay_between_joining = delay_between_joining
        # is the client have spam block for sending messages
        self.__group_send_spam: bool = False
        # time till account should wait before sending
        self.__group_send_timeout: float = None
        # self information
        self.__self_info: InputPeerUser = None

    async def print_respond(self, respond: Respond):
        """prints text

        Args:
            respond (Respond): respond from function.
        """
        success_word = "Успешно" if respond.get_success() else "Ошибка"

        add_text = respond.get_respond_text()
        add_text = add_text if add_text is not None else ""

        phone = self.__self_info.phone

        result_string = f"{phone} {success_word}. {add_text}"
        print(result_string)

    async def processing_join_groups(self):
        """main process for entering groups
        """
        while True:
            if self.__group_enter_spam:
                # if we have spam block -> recheck it
                current_time = datetime.now().timestamp()
                if current_time < self.__group_enter_timeout:
                    # if we still need to wait
                    time_to_sleep = self.__group_enter_timeout - current_time

                    print(
                        f"{self.__self_info.phone} ожидание окончания \
спам-блока на вступление ({time_to_sleep} сек)")

                    await asyncio.sleep(time_to_sleep)
                    continue  # go to next iteration
                else:
                    # if no more spam -> go further
                    self.__group_enter_spam = False

            respond = await self.join_group()
            await self.print_respond(respond)

            if isinstance(respond.get_respond_exception(), IndexError):
                # if no more groups to enter
                return

            await self.__wait_random_time(5, 20)  # delay between join

    async def join_group(self) -> Respond:
        """choose group to join and use neccessary algorithms with respect to \
            type of group.

        Returns:
            Respond: respond of function.
        """
        # try to get group to join
        try:
            group_to_join = self.__receive_group_to_join()
        except IndexError as e:
            # no groups in group list

            phone = self.__self_info.phone
            return Respond(
                success=False,
                respond_text=f"{phone} Закончились группы для вступления",
                respond_exception=e
            )
        else:
            # no error

            print(
                f"{self.__self_info.phone} вступает в {group_to_join}")

            if "joinchat" in group_to_join:
                # if link contains joinchat -> its private.
                # transform it to "+" private link
                group_to_join = group_to_join.replace(
                    "joinchat/",
                    "+"
                )

            # choose private or public group
            if "+" in group_to_join:
                # if "+" in link -> it's private group
                respond = await self.__join_private_group(group_to_join)

                if respond.get_success()\
                        and respond.get_respond_exception() is None:
                    # if we enter group and no errors -> first time in group

                    # wait before capcha click
                    await self.__wait_random_time(1.2, 2.2)
                    await self.__pass_capcha(group_to_join)  # click capcha

                return respond
            else:
                # IN NEXT UPDATES
                return Respond(
                    success=False,
                    respond_text="Программа не работает с открытыми группами."
                )

    async def processing_send_groups_message(self):
        """Main process for sending messages
        """
        self.__timeout_before_sending = self.__calculate_timeout(
            self.__delay_before_sending)

        while True:
            current_time = datetime.now().timestamp()

            if current_time < self.__timeout_before_sending:
                # if we need to wait before sending
                time_to_wait = self.__timeout_before_sending - current_time

                print(
                    f"{self.__self_info.phone} ожидание перед рассылкой \
({time_to_wait} сек).")

                await asyncio.sleep(time_to_wait)
                continue

            async for dialog in self.telethon_client.iter_dialogs():
                dialog: Dialog

                await self.__wait_random_time(1, 2)

                # check is it a group
                if dialog.id > 0:
                    # if id positive -> it's not a group
                    continue

                # check spam
                if self.__group_send_spam:
                    # if we have spam -> rechek it
                    current_time = datetime.now().timestamp()
                    if current_time < self.__group_send_timeout:
                        # if we still need to wait
                        time_to_sleep = self.__group_send_timeout \
                            - current_time

                        print(f"{self.__self_info.phone} ожидание окончания \
спам-блока на рассылку ({time_to_sleep} сек).")

                        await asyncio.sleep(time_to_sleep)
                    else:
                        # if no more spam -> go further
                        self.__group_send_spam = False

                respond = await self.send_message_to_group(dialog)
                # sleep between each sending
                await self.__wait_random_time(10, 20)
                await self.print_respond(respond)

            phone = self.__self_info.phone
            print(
                f"{phone} Пауза перед следующей отправкой.")
            await self.__wait_random_time(
                self.__delay_between_sending-10,
                self.__delay_between_sending+10
            )

    async def send_message_to_group(self, dialog: Dialog) -> Respond:
        print(f"{self.__self_info.phone} пишет в {dialog.title}")

        message = await self.__get_my_message()
        await self.__wait_random_time(1, 3)
        # try to send message
        try:
            await self.telethon_client.forward_messages(
                dialog, message
            )
        except (FloodWaitError, PeerFloodError) as e:
            # if we have spam block

            # set spam parameters
            self.__group_send_timeout = self.__calculate_timeout(e.seconds)
            self.__group_send_spam = True

            return Respond(
                success=False,
                respond_text="Аккаунт во временном спам-блоке.",
                respond_exception=e
            )
        except Exception as e:
            # leave dialog if its not a spam

            # some delay before leaving group
            await self.__wait_random_time(0.2, 1.2)
            await self.telethon_client.delete_dialog(dialog)

            return Respond(
                success=False,
                respond_text="Непредвиденная ошибка при отправке. \
Покидаем группу.",
                respond_exception=e
            )
        else:
            # if no errors
            return Respond()

    async def get_self_information(self) -> InputPeerUser:
        """get self information and assign it to the
        parameter of NewClient

        Returns:
            InputPeerUser: self information
        """
        self_info = await self.telethon_client.get_me()
        await asyncio.sleep(1)
        self.__self_info = self_info
        return self_info

    async def processing_ping_server(self):
        """process that pings server every 10sec to prevent sleep"""
        while True:
            await self.telethon_client(GetStateRequest())
            await asyncio.sleep(10)

    # ------- PRIVATE ------ #

    def __receive_group_to_join(self) -> str:
        """gets next group to join from list.

        Returns:
            str: link to group.

        Raises:
            IndexError: if no elements in list.
        """
        next_group = self.__groups_to_join.pop(0)
        return next_group

    async def __join_private_group(self, group_link: str) -> Respond:
        invite_hash = self.__parse_private_link(group_link)

        # try to enter private group
        try:
            await self.telethon_client(ImportChatInviteRequest(invite_hash))
        except (FloodWaitError, PeerFloodError) as e:
            # if we have spam-block

            delay = self.__get_wait_time_from_error(e)

            # set group spams and delays
            self.__group_enter_spam = True
            self.__group_enter_timeout = self.__calculate_timeout(
                delay)

            return Respond(
                success=False,
                respond_text="Аккаунт в спам блоке для вступления в группу.",
                respond_exception=e
            )
        except UserAlreadyParticipantError as e:
            # if user is already in group
            return Respond(
                success=True,
                respond_text="Аккаунт уже состоит в этой группе.",
                respond_exception=e
            )
        except InviteHashExpiredError as e:
            # if link is expired
            return Respond(
                success=False,
                respond_text="Ссылка истекла и больше не действительна.",
                respond_exception=e
            )
        except ChannelsTooMuchError as e:
            # if user entered too much groups
            return Respond(
                success=False,
                respond_text="Аккаунт состоит в слишком большом количестве "
                + "каналов.",
                respond_exception=e
            )
        except InviteRequestSentError as e:
            # if we need to send join request
            return Respond(
                success=False,
                respond_text="Для вступления требуется отправить заявку.",
                respond_exception=e
            )
        except Exception as e:
            # some unpredected error
            return Respond(
                success=False,
                respond_text="Возникла непредвиденная ошибка:\n"
                + f"{traceback.format_exc()}",
                respond_exception=e
            )
        else:
            # if no error
            return Respond(
                success=True
            )

    def __parse_private_link(self, group_link: str) -> str:
        """parses private links to get hash from them.

        Args:
            group_link (str): link to private group

        Returns:
            str: invite hash
        """
        invite_hash = group_link.replace("https://t.me/+", "")
        return invite_hash

    def __get_wait_time_from_error(
        self, e: Union[FloodWaitError, PeerFloodError]
    ) -> int:
        """gets amount of seconds to wait because of spam.

        Args:
            e (Union[FloodWaitError, PeerFloodError]): exception from telethon

        Returns:
            int: amount of seconds to wait.
        """
        return e.seconds

    def __calculate_timeout(self, delay: int) -> float:
        """gets delay and current time and calculates time till we need to \
            wait before entering a group.

        Args:
            delay (int): seconds to wait.

        Returns:
            float: timestamp till wait.
        """
        current_time = datetime.now().timestamp()
        group_enter_timeout = current_time + delay

        return group_enter_timeout

    async def __pass_capcha(self, group: str) -> None:
        try:
            messages = await self.telethon_client.get_messages(
                group.replace("+", "joinchat/"), limit=10)

            for i in range(10):
                try:
                    await messages[i].click()
                except Exception:
                    pass

        except Exception:
            pass

    async def __get_my_message(self) -> Message:
        messages = await self.telethon_client.get_messages("me", limit=1)
        message: Message = messages[0]
        return message

    async def __wait_random_time(self, left_bound: float, right_bound: float):
        """provides the async sleep for the random delay
        from left to right bound

        Args:
            left_bound (float): left bound of range of seconds
            right_bound (float): right bound of range of seconds
        """
        seconds_to_wait = uniform(left_bound, right_bound)
        await asyncio.sleep(seconds_to_wait)

    # ------- Getters ------ #
    def get_groups_to_join(self) -> List[str]:
        return self.__groups_to_join.copy()

    def get_telethon_client(self) -> TelegramClient:
        return self.telethon_client
