"""
This is a script for joining and sending messages
to Telegram groups automatically by what_is_love.

Vladislav Bolshakov (c). All rights reserved.
"""


# standart library
import asyncio
import sys

# standart library
from typing import List, Tuple, Dict
from random import randint
from time import sleep

# global imports
from telethon import TelegramClient

# local imports
from lib.new_client import NewClient


class Main:
    @staticmethod
    async def main():
        list_of_clients = await Main.read_accounts_from_file()
        list_of_chats = Main.read_links_from_file()

        new_clients = await Main.split_links_by_clients(
            list_of_clients, list_of_chats)

        loop = asyncio.get_event_loop()
        for new_client in new_clients:
            await new_client.telethon_client.connect()
            await new_client.get_self_information()
            task = loop.create_task(new_client.processing_join_groups())
            task1 = loop.create_task(
                new_client.processing_send_groups_message())
            task2 = loop.create_task(new_client.processing_ping_server())

    @staticmethod
    async def split_links_by_clients(
            list_of_clients: List[TelegramClient],
            list_of_links: List[str]) -> List[NewClient]:

        number_of_clients = len(list_of_clients)

        new_clients_list: List[NewClient] = []

        input_dict = Main.read_input()

        for i in range(number_of_clients):
            await list_of_clients[i].connect()
            self_info = await list_of_clients[i].get_me()
            phone = self_info.phone
            await list_of_clients[i].disconnect()
            delay_before_sending = int(input(
                f"Введите задержку перед началом отправления (в минутах) для \
{phone}: "
            ))

            new_client = NewClient(
                telethon_client=list_of_clients[i],
                groups_to_join=list_of_links.copy(),
                delay_before_sending=delay_before_sending,
                delay_between_joining=input_dict["delay_between_join"],
                delay_between_sending=input_dict["delay_between_sending"]
            )
            new_clients_list.append(new_client)

        return new_clients_list.copy()

    @staticmethod
    def read_links_from_file() -> List[str]:
        """read links line-by-line from file.

        Returns:
            List[str]: list of links
        """
        list_of_links = []
        with open("links.txt") as f:
            for line in f:
                list_of_links.append(line.strip())

        return list_of_links.copy()

    @staticmethod
    def read_api_id_and_hash() -> Tuple[str]:
        """reads api_id and api_hash from file.

        Returns:
            Tuple[str]: id and hash
        """
        with open("config.txt") as f:
            for line in f:
                if "api_id" in line:
                    api_id = line.split(":")[1].strip()
                elif "api_hash" in line:
                    api_hash = line.split(":")[1].strip()
        return (api_id, api_hash)

    @staticmethod
    async def read_accounts_from_file() -> List[TelegramClient]:
        list_of_accounts = []

        api_id, api_hash = Main.read_api_id_and_hash()
        api_id = int(api_id)

        with open("accounts.txt") as f:
            for line in f:
                if line.strip() == "":
                    # if line is empty
                    continue

                line_elements = line.split()
                phone = line_elements[0]

                # choose proxy from list randomly
                proxys = line_elements[1::]
                if proxys:
                    # if we have proxy
                    index = randint(0, len(proxys)-1)
                    proxy = proxys[index].split(":")

                    # split proxy
                    if len(proxy) == 5:
                        formated_proxy = {
                            'proxy_type': proxy[0],
                            'addr': proxy[1],
                            'port': int(proxy[2]),
                            'username': proxy[3],
                            'password': proxy[4]
                        }
                    elif len(proxy) == 3:
                        formated_proxy = {
                            'proxy_type': proxy[0],
                            'addr': proxy[1],
                            'port': int(proxy[2]),
                        }
                else:
                    formated_proxy = None

                client = TelegramClient(
                    phone, api_id, api_hash, proxy=formated_proxy)

                try:
                    await Main.sing_in(client, line)
                except Exception as e:
                    print(f"Ошибка при подключении. {e}")
                else:
                    list_of_accounts.append(client)

        return list_of_accounts.copy()

    @staticmethod
    async def sing_in(client: TelegramClient, phone: str) -> TelegramClient:
        print(f"Подключаемся к {phone}")
        await client.connect()

        try:
            if not await client.is_user_authorized():
                await client.send_code_request(phone)
                code = input("Введите полученный код: ")
                await client.sign_in(phone, code)
        except Exception as e:
            await client.disconnect()
            raise e
        else:
            await client.disconnect()

    @staticmethod
    def read_input() -> Dict[str, int]:
        delay_between_sending = input(
            "Введите паузу между циклами отправлений в группы (в секундах): "
        )
        try:
            delay_between_sending = int(delay_between_sending)
        except Exception:
            print("НЕПРАВИЛЬНЫЙ ВВОД")
            sleep(3)
            sys.exit(1)

        result_dict = {
            "delay_between_join": 0,
            "delay_between_sending": delay_between_sending}

        return result_dict


if __name__ == "__main__":
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
    loop = asyncio.get_event_loop()
    loop.create_task(Main.main())
    loop.run_forever()
